<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Set;

/**
 * A Country
 *
 * @property int $id
 * @property string $code
 * @property string $name_fr
 * @property string $name_en
 */
class Country extends \app_Record
{
    
    /**
     * overload default record title
     *
     * (non-PHPdoc)
     *
     * @see \ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->getName();
    }
    
    /**
     * Name of country according to language
     *
     * @return string
     */
    public function getName()
    {
        $colname = 'name_en';
        
        if ('fr' === bab_getLanguage()) {
            $colname = 'name_fr';
        }
        
        return $this->$colname;
    }
    
    /**
     * Map a geonames country into a CRM contry record
     *
     * @param \ORM_Record $geoNameCountry
     */
    public function geoNamesMap(\ORM_Record $geoNameCountry)
    {
        $this->code = $geoNameCountry->iso;
        $this->name_en = $geoNameCountry->country_en;
        $this->name_fr = $geoNameCountry->country_fr;
    }
    
    public function getContinentCode()
    {
        $gn = \bab_functionality::get('GeoNames');
        if (!$gn) {
            return '';
        }
        
        $set = $gn->getCountryOrmSet();
        $country = $set->get($set->iso->is($this->code));
        if($country){
            return $country->continent;
        }
        
        return '';
    }
}
