<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Set;

use bab_functionality;

/**
 * A CountrySet
 *
 * @property ORM_PkField $id
 * @property ORM_StringField $code
 * @property ORM_StringField $name_en
 * @property ORM_StringField $name_fr
 *
 */
class CountrySet extends \app_RecordSet
{
    
    public function __construct(\Func_App $App = null)
    {
        $this->setTableName($App->classPrefix.'Country');
        parent::__construct($App);
        $this->setDescription('Country');
        $this->setPrimaryKey('id');
        $appC = $App->getComponentByName('Country');
        
        $this->addFields(
            ORM_StringField('code', 2)->setDescription($appC->translate('ISO code')),
            ORM_StringField('name_en', 255)->setDescription($appC->translate('Name (english)')),
            ORM_StringField('name_fr', 255)->setDescription($appC->translate('Name (french)'))
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new CountryBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new CountryAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function onUpdate()
    {
        $App = $this->App();
        $component = $App->getComponentByName('Country');
        $countrySet = $App->CountrySet();
        if ($countrySet->select()->count() == 0 ) {
            if($countrySet->populateFromGeoNames()) {
                $message = $component->translate('Countries database initialized');
                $color = 'green';
            }
            else{
                $message = $component->translate('Countries database could not be initialized. Please ensure the library libgeonames is installed');
                $color = 'red';
            }
            $message = "<span style='color:{$color};'>{$message}</span>";
            \bab_installWindow::message($message);
        }
    }
    
    /**
     *
     * @return string
     */
    public function getNameColumn()
    {
        $colname = 'name_en';
        if ('fr' === $GLOBALS['babLanguage']) {
            $colname = 'name_fr';
        }
        
        return $colname;
    }
    
    public function getNameField()
    {
        $name = $this->getNameColumn();
        return $this->$name;
    }
    
    /**
     * Get countries as array
     */
    public function getOptions()
    {
        $colname = $this->getNameColumn();
        
        $options = array(
            0 => ''
        );
        $iterator = $this->select()->orderAsc($this->$colname);
        
        foreach ($iterator as $country) {
            $options[$country->id] = $country->$colname;
        }
        
        return $options;
    }
    
    /**
     * populate database with geoNames data
     *
     * @return bool
     */
    public function populateFromGeoNames()
    {
        $gn = @bab_functionality::get('GeoNames');
        if (! $gn) {
            return false;
        }
        
        $iterator = $gn->getCountryOrmSet()->select();
        
        foreach ($iterator as $country) {
            $record = $this->newRecord();
            $record->geoNamesMap($country);
            $record->save();
        }
        
        return true;
    }
    
    /**
     * Guess country from browser language
     *
     * @param $country_code string
     *            Default country code to use if nothing found
     * @return Country | null
     */
    public function guessCountryFromBrowser($country_code = null)
    {
        $langs = array();
        
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if (count($lang_parse[1])) {
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                foreach ($langs as $lang => $val) {
                    if ($val === '')
                        $langs[$lang] = 1;
                }
                arsort($langs, SORT_NUMERIC);
            }
        }
        
        // try to get a 2 char country code
        
        foreach ($langs as $code => $dummy) {
            if (2 === strlen($code)) {
                $country_code = mb_strtoupper($code);
                break;
            }
            
            if (false !== mb_strpos($code, '-')) {
                
                list ($language, $country_code) = explode('-', $code);
                $country_code = mb_strtoupper($country_code);
                break;
            }
        }
        
        if (null !== $country_code) {
            return $this->get($this->code->is($country_code));
        }
        
        return null;
    }
}

class CountryBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class CountryAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}