<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Set;

use ORM_Record;
use bab_Registry;
use bab_functionality;

/**
 * A app_Address is any kind of postal address.
 *
 * @property string $recipient
 * @property string $street
 * @property string $postalCode
 * @property string $city
 * @property string $cityComplement
 * @property string $state
 * @property string $longitude
 * @property string $latitude
 * @property string $instructions
 * @property Country $country
 *
 */
class Address extends \app_TraceableRecord
{
    
    /**
     * return true if the address is empty
     *
     * @return bool
     */
    public function isEmpty()
    {
        return '' == $this->street && '' == $this->city && '' == $this->postalCode && (null == $this->country || '0' == $this->country || ($this->country instanceof Country && '' == $this->country->getName()));
    }
    
    private function getAddressParts($defaultCountry = null)
    {
        $addressParts = array();
        if ($this->street) {
            $addressParts[] = $this->street;
        }
        
        $addressParts[] = trim($this->postalCode . ' ' . $this->city . ' ' . $this->cityComplement);
        
        $countryName = $defaultCountry;
        if (null !== $country = $this->getCountry()) {
            if ($country->getName()) {
                $countryName = $country->getName();
            }
        }
        
        if (isset($countryName)) {
            $addressParts[] = $countryName;
        }
        
        return $addressParts;
    }
    
    /**
     *
     * @return string
     */
    public function toString()
    {
        return implode("\n", $this->getAddressParts());
    }
    
    /**
     *
     * {@inheritdoc}
     * @see ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->toString();
    }
    
    /**
     * Get string optimized for the google address suggest
     */
    public function getSuggest($defaultCountry = null)
    {
        return implode(", ", $this->getAddressParts($defaultCountry));
    }
    
    /**
     *
     * @return Country | null
     */
    public function getCountry()
    {
        if ($this->country instanceof Country) {
            return $this->country();
        }
        
        if (! empty($this->country)) {
            $set = $this->App()->CountrySet();
            return $set->get($set->id->is($this->country));
        }
        
        return null;
    }
    
    /**
     * Update properties with CSV row
     *
     * @param \Widget_CsvRow $row
     * @param string $prefix
     *            prefix string used on each property of address in $row
     *
     * @return int
     */
    public function import(\Widget_CsvRow $row, $prefix = '')
    {
        $up_prop = 0;
        
        static $CSet = null;
        static $countries = array();
        
        $street = $prefix . 'street';
        $postalCode = $prefix . 'postalCode';
        $city = $prefix . 'city';
        $cityComplement = $prefix . 'cityComplement';
        $state = $prefix . 'state';
        $country = $prefix . 'country';
        
        if (isset($row->$street)) {
            $up_prop += $this->importProperty('street', $row->$street);
        }
        
        if (isset($row->$postalCode)) {
            $pc = str_replace(' ', '', $row->$postalCode);
            $up_prop += $this->importProperty('postalCode', $pc);
        }
        
        if (isset($row->$city)) {
            $cedexPos = strpos(strtoupper($row->$city), 'CEDEX');
            if ($cedexPos) {
                $cityName = substr($row->$city, 0, $cedexPos - 1);
                $cityCedex = substr($row->$city, $cedexPos);
                $up_prop += $this->importProperty('city', $cityName);
                $up_prop += $this->importProperty('cityComplement', $cityCedex);
            } else {
                $up_prop += $this->importProperty('city', $row->$city);
            }
        }
        
        if (isset($row->$cityComplement)) {
            $up_prop += $this->importProperty('cityComplement', $row->$cityComplement);
        }
        
        if (isset($row->$state)) {
            $up_prop += $this->importProperty('state', $row->$state);
        }
        
        if (isset($row->$country)) {
            if (null === $CSet) {
                $CSet = $this->App()->CountrySet();
            }
            
            if (! isset($countries[$row->$country])) {
                $CRecord = $CSet->get($CSet->name_fr->like($row->$country)
                    ->_OR_($CSet->name_en->like($row->$country)));
                if (! $CRecord) {
                    $countries[$row->$country] = '0';
                } else {
                    $countries[$row->$country] = $CRecord->id;
                    
                    $CRecord->__destruct();
                    unset($CRecord);
                }
            }
            
            $up_prop += $this->importProperty('country', $countries[$row->$country]);
        }
        
        return $up_prop;
    }
    
    /**
     * Get geocoded longitude and latitude of the current address
     *
     * @return mixed false|array False if geoname is not accessible or the address is empty or an array('longitude', 'latitude')
     */
    public function getCoordinates($debug = false)
    {
        $App = $this->App();
        if ($this->city == '' && $this->postalCode == '' && $this->street == '') {
            return false;
        }
        
        $gn = @bab_functionality::get('GeoNames');
        if (! $gn) {
            return false;
        }
        
        $country = null;
        if (isset($App->Country)) {
            if ($this->country instanceof Country) {
                $country = $this->country->name_fr;
            } elseif ($country = $App->CountrySet()->get($this->country)) {
                $country = $country->name_fr;
            }
        }
        
        return $gn->getAddressCoordinates($this->city, $this->postalCode, $this->street, $country, $debug);
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecord::save()
     */
    public function save($noTrace = false)
    {
        $useCDN = bab_Registry::get('/core/useCDN', true);
        if ($useCDN) {
            $coordinates = $this->getCoordinates();
            if ($coordinates) {
                $this->latitude = $coordinates['latitude'];
                $this->longitude = $coordinates['longitude'];
            } else {
                $this->latitude = '-';
                $this->longitude = '-';
            }
        }
        return parent::save($noTrace);
    }
    
    public function setDataGouvInputValues(String $string){
        $values = explode("|",$string);
        $this->street = $values[0];
        $this->postalCode = $values[1];
        $this->city = $values[2];
        $countrySet = $this->App()->CountrySet();
        if($countrySet){
            $this->state = $countrySet->get($countrySet->name_fr->like($values[3]));
        }
        return $this;
    }
}
