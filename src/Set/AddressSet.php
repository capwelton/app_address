<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Set;

/**
 * A app_Address is any kind of postal address.
 *
 * @property ORM_TextField $recipient
 * @property ORM_TextField $street
 * @property ORM_StringField $postalCode
 * @property ORM_StringField $city
 * @property ORM_StringField $cityComplement
 * @property ORM_StringField $state
 * @property ORM_StringField $longitude
 * @property ORM_StringField $latitude
 * @property ORM_TextField $instructions
 * @property app_CountrySet $country
 *
 * @method app_Address get(mixed $criteria)
 * @method app_Address request(mixed $criteria)
 * @method app_Address[]|\ORM_Iterator select(\ORM_Criteria $criteria)
 * @method app_Address newRecord()
 *
 */
class AddressSet extends \app_TraceableRecordSet
{
    /**
     *
     * @param
     *            Func_App App()
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName($App->classPrefix.'Address');
        $App = $this->App();
        $appC = $App->getComponentByName('Address');
        $this->setDescription('Address');
        $this->addFields(
            ORM_TextField('street')->setDescription($appC->translate('Number / pathway')),
            ORM_StringField('postalCode', 10)->setDescription($appC->translate('Zip code')),
            ORM_StringField('city', 60)->setDescription($appC->translate('City')),
            ORM_StringField('cityComplement', 60)->setDescription($appC->translate('City complement (CEDEX...)')),
            ORM_StringField('state', 60)->setDescription($appC->translate('State/Region')),
            ORM_StringField('longitude', 60)->setDescription($appC->translate('Longitude')),
            ORM_StringField('latitude', 60)->setDescription($appC->translate('Latitude'))
        );
        
        $this->hasOne('country', $App->CountrySetClassName());
    }
    
    public function getRequiredComponents()
    {
        return array(
            'Country'
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new AddressBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new AddressAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
}

class AddressBeforeSaveEvent extends \RecordAfterSaveEvent
{
    
}

class AddressAfterSaveEvent extends \RecordBeforeSaveEvent
{
    
}