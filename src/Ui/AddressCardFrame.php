<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Address\Ui;

use Capwelton\App\Address\Set\Address;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');


class AddressCardFrame extends \app_CardFrame
{
    /**
     *
     * @param \Func_App $App
     * @param Address $address
     * @param string $title
     * @param string $recipient			Deprecated parameter, if specified, overwrite the recipient stored in address object
     */
    public function __construct(\Func_App $App, Address $address, $title = null, $recipient = null)
    {
        $W = bab_Widgets();
        $layout = $W->VBoxLayout()->SetVerticalSpacing(.3, 'em');
        
        if ($title) {
            $layout = $this->LabelStr($title, $layout);
        }
        
        parent::__construct($App, null, $layout->addClass('app-titled-address'));
        
        
        if ($recipient) {
            $address->recipient = $recipient;
        }
        
        $addressFrame = new AddressFrame($address);
        //$addressFrame->addClass('widget-nowrap'); //SHOULD NOT BE FORCE BECAUSE WE CANNOT REMOVE IT
        $addressFrame->setSizePolicy(\Widget_SizePolicy::MINIMUM);
        
        
        $layout->addItem($addressFrame);
        
        if ($maplink = $this->MapLink(null, $address)) {
            
            $menu = $W->Menu(null, $W->FlowLayout())
            ->attachTo($addressFrame)
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addItem($maplink);
            
            //$maplink->addClass('app-address-map');
            $layout->addItem($menu);
        }
    }
}