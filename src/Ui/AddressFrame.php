<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Ui;

use Capwelton\App\Address\Set\Address;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

class AddressFrame extends \Widget_Frame
{
    
    const MAPS_URL = '//maps.google.com/?q=%s';
    
    const STATIC_MAPS_URL = '//maps.googleapis.com/maps/api/staticmap?';
    
    protected $address;
    
    private $Widgets;
    
    public function __construct(Address $address, $id = null, \Widget_Layout $layout = null)
    {
        $W = bab_Widgets();
        $this->Widgets = $W;
        
        if (! isset($layout)) {
            $layout = $W->VBoxLayout();
        }
        parent::__construct($id, $layout);
        
        $this->address = $address;
        
        if (! empty($address->recipient)) {
            $this->addItem($W->RichText($address->recipient)
                ->setRenderingOptions(BAB_HTML_BR)
                ->addClass('app-strong')
                ->addClass('fn')
                ->addClass('recipient'));
        }
        $this->addItem($W->RichText($address->street)
            ->setRenderingOptions(BAB_HTML_BR)
            ->addClass('street-address'));
        $this->addItem($W->FlowItems($W->Label($address->postalCode)
            ->addClass('postal-code'), $W->Label($address->city)
            ->addClass('locality'), $W->Label($address->cityComplement))
            ->setHorizontalSpacing(1, 'ex'));
        
        $this->addClass('adr');
        
        $this->addCountry();
        $this->addMapLink();
        $this->addInstruction();
    }
    
    public function addInstruction()
    {
        $W = $this->Widgets;
        
        if ($this->address->instructions) {
            $this->addItem($W->VBoxItems($W->Label(app_translate('Additional instructions'))
                ->colon(), $W->RichText($this->address->instructions)
                ->setRenderingOptions(BAB_HTML_BR))
                ->addClass('widget-small widget-bordered'));
        }
        
        return $this;
    }
    
    /**
     * Add country
     *
     * @return AddressFrame
     */
    public function addCountry()
    {
        $W = $this->Widgets;
        
        if ($country = $this->address->getCountry()) {
            
            $label = $W->Label($country->getName())
            ->addClass('country');
            
            if ($country->code) {
                if ($flag = $W->CountryFlag($country->code)) {
                    $label = $W->FlowItems($flag, $label)
                    ->setHorizontalSpacing(.3, 'em')
                    ->setVerticalAlign('middle');
                }
            }
            
            $this->addItem($label);
        }
        
        return $this;
    }
    
    /**
     * Adds a popup menu containing a static map of the address.
     *
     * @return AddressFrame
     */
    public function addMapLink()
    {
        $W = $this->Widgets;
        if ($maplink = $this->MapLink(null, $this->address)) {
            
            $menu = $W->Menu(null, $W->FlowLayout())
            ->attachTo($this)
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addItem($maplink);
            
            $this->addItem($menu);
        }
        
        return $this;
    }
    
    /**
     * Adds a popup menu containing a static map of the address.
     *
     * @return AddressFrame
     */
    public function addMapPreview($zoom = 15, $width = 200, $height = 150)
    {
        if ($maplink = $this->MapLink(null, $this->address, $zoom, $width, $height)) {
            $this->addItem($maplink);
        }
        
        return $this;
    }
    
    /**
     *
     * @return \Widget_Link | null
     */
    public function MapLink($name = null, Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        if (empty($address->city)) {
            return null;
        }
        
        $query = '';
        
        if (! empty($address->street)) {
            $query = str_replace("\n", ' ', $address->street);
            $query = str_replace("\r", ' ', $address->street);
        }
        
        if (isset($name)) {
            $query = $name . ', ' . $query;
        }
        
        $query .= $address->postalCode ? ', ' . $address->postalCode : '';
        $query .= ', ' . $address->city;
        // $query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', ' . $country->getName();
        }
        
        $url = sprintf(self::MAPS_URL, urlencode($query));
        $W = $this->Widgets;
        
        return $W->Link($this->MapLinkDisplay($address, $zoom, $width, $height), $url)
        ->setOpenMode(\Widget_Link::OPEN_POPUP);
    }
    
    /**
     * Returns an url to an image of a map corresponding to the specified address.
     *
     * @param Address $address
     *            The address to display on a map.
     * @param int $zoom
     *            Zoom level.
     * @param int $width
     *            Image width in pixels.
     * @param int $height
     *            Image height in pixels.
     *
     * @return string
     */
    protected function staticMapUrl(Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        if (empty($address->city)) {
            return null;
        }
        
        $query = '';
        
        if (empty($address->street)) {
            $zoom = 11;
        } else {
            $query = str_replace("\n", ' ', $address->street);
            $query = str_replace("\r", ' ', $address->street);
        }
        $query .= $address->postalCode ? ', ' . $address->postalCode : '';
        $query .= ', ' . $address->city;
        // $query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', ' . $country->getName();
        }
        
        $url = self::STATIC_MAPS_URL . 'zoom=' . $zoom . '&size=' . $width . 'x' . $height . '&maptype=roadmap&sensor=false&markers=color:green|' . urlencode(bab_removeDiacritics($query));
        $url .= isset($GLOBALS['babGoogleApiKey']) ? '&key=' . $GLOBALS['babGoogleApiKey'] : '';
        return $url;
    }
    
    /**
     *
     * @return \Widget_Image
     */
    protected function MapLinkDisplay(Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        $W = $this->Widgets;
        
        return $W->Image($this->staticMapUrl($address, $zoom, $width, $height));
    }
}