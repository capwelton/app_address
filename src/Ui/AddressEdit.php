<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Address\Ui;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * AddressEdit is a compound input widget usable for editing a postal address.
 */
class AddressEdit extends \app_UiObject
{
    
    protected $showRecipient = false;
    
    protected $showInstructions = false;
    
    protected $showCountry = true;
    
    protected $showComplement = true;
    
    protected $showMapPreview = true;
    
    protected $showCoordones = false;
    
    protected $showLabels = true;
    
    protected $mandatory = false;
    
    protected $mandatoryMessage = '';
    
    protected $mandatoryStreet = false;
    
    protected $mandatoryPostalCode = false;
    
    protected $mandatoryCity = false;
    
    protected $mandatoryComplement = false;
    
    protected $mandatoryCountry = false;
    
    protected $defaultCountry = null;
    
    protected $convertCase = null;
    
    protected $addressComponent = null;
    /**
     *
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($App);
        
        $this->addressComponent = $App->getComponentByName('Address');
        
        $W = bab_Widgets();
        
        $this->setInheritedItem($W->NamedContainer(null, $id)
            ->addClass('adr'));
        
        $countrySet = $App->CountrySet();
        $france = $countrySet->get($countrySet->code->is('FR'));
        
        if($france){
            $this->setDefaultCountry($france->id);
        }
    }
    
    /**
     * Defines if the recipient field should be visible or not
     *
     * @param bool $showRecipient
     * @return AddressEdit
     */
    public function showRecipient($showRecipient = true)
    {
        $this->showRecipient = $showRecipient;
        return $this;
    }
    
    /**
     * Definie if the instructions field should be visible or not
     *
     * @param bool $showInstructions
     * @return AddressEdit
     */
    public function showInstructions($showInstructions = true)
    {
        $this->showInstructions = $showInstructions;
        return $this;
    }
    
    /**
     * Defines if the labels must be displayed above the fields (if not they are used as placeholders).
     *
     * @param bool $showLabels
     * @return self
     */
    public function showLabels($showLabels = true)
    {
        $this->showLabels = $showLabels;
        return $this;
    }
    
    /**
     * Defines if the country selector should be visible or not.
     *
     * @param bool $showCountryField
     *            True to have the country selector in the form.
     *
     * @return AddressEdit
     */
    public function showCountry($showCountryField = true)
    {
        $this->showCountry = $showCountryField;
        return $this;
    }
    
    public function setDefaultCountry($defaultCountry)
    {
        $this->defaultCountry = $defaultCountry;
        return $this;
    }
    
    /**
     * Defines if the address complement field should be visible or not.
     * The complement field is a short line edit after the city field wihich typically
     * contains the optional CEDEX information in France.
     *
     * @param bool $showComplementField
     *            True to have the postal code complement field (CEDEX...) in the form.
     *
     * @return AddressEdit
     */
    public function showComplement($showComplementField = true)
    {
        $this->showComplement = $showComplementField;
        return $this;
    }
    
    /**
     * Defines if an automatic map preview should be visible or not.
     *
     * @param bool $showMapPreview
     *            True to have the map preview in the form.
     *
     * @return AddressEdit
     */
    public function showMapPreview($showMapPreview = true)
    {
        $this->showMapPreview = $showMapPreview;
        return $this;
    }
    
    /**
     * Defines if an automatic map preview should be visible or not.
     *
     * @param bool $showMapPreview
     *            True to have the map preview in the form.
     *
     * @return AddressEdit
     */
    public function showCoordones($showCoordones = true)
    {
        $this->showCoordones = $showCoordones;
        return $this;
    }
    
    /**
     * Makes address fields mandatory.
     *
     * @param bool $mandatory
     *            True to make address fields mandatory.
     * @param string $message
     *
     * @return AddressEdit
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        $this->mandatory = $mandatory;
        $this->mandatoryStreet = $mandatory;
        $this->mandatoryPostalCode = $mandatory;
        $this->mandatoryCity = $mandatory;
        
        return $this;
    }
    
    /**
     *
     * @param bool $mandatory
     * @return AddressEdit
     */
    public function setCountryMandatory($mandatory = true)
    {
        $this->mandatoryCountry = $mandatory;
        return $this;
    }
    
    /**
     *
     * @param bool $mandatory
     * @return AddressEdit
     */
    public function setStreetMandatory($mandatory = true)
    {
        $this->mandatoryStreet = $mandatory;
        return $this;
    }
    
    /**
     *
     * @param bool $mandatory
     * @return AddressEdit
     */
    public function setPostalCodeMandatory($mandatory = true)
    {
        $this->mandatoryPostalCode = $mandatory;
        return $this;
    }
    
    /**
     *
     * @param bool $mandatory
     * @return AddressEdit
     */
    public function setCityMandatory($mandatory = true)
    {
        $this->mandatoryCity = $mandatory;
        return $this;
    }
    
    /**
     *
     * @param bool $mandatory
     * @return AddressEdit
     */
    public function setComplementMandatory($mandatory = true)
    {
        $this->mandatoryComplement = $mandatory;
        return $this;
    }
    
    /**
     *
     * @param \Widget_Canvas $canvas
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->initialize();
        $displayable = parent::display($canvas);
        if($this->showMapPreview){
            $displayable .= $canvas->loadScript($this->getId(), $this->App()->getComponentByName('Address')->getScriptPath().'addresspreview.js'.'?'.microtime(true));
        }
        return $displayable;
    }
    
    /**
     * Sets the value of the text transformation for the city field.
     * Currently only uppercase / lowercase transformation.
     *
     * @param int $convertCase
     *            One of MB_CASE_LOWER, MB_CASE_UPPER or MB_CASE_TITLE.
     */
    public function setConvertCase($convertCase)
    {
        $this->convertCase = $convertCase;
        return $this;
    }
    
    /**
     *
     * @param string $labelText
     * @param \Widget_Displayable_Interface $inputWidget
     * @param string $descriptionText
     */
    protected function formField($labelText, \Widget_Displayable_Interface $inputWidget, $descriptionText = null)
    {
        $W = bab_Widgets();
        
        $formField = $W->VBoxLayout()->setVerticalSpacing(0.2, 'em');
        
        if ($inputWidget instanceof \Widget_CheckBox) {
            $formField->addItem($W->HBoxItems($inputWidget, $W->Label($labelText)
                ->setAssociatedWidget($inputWidget))
                ->setVerticalAlign('middle'));
        } else {
            if ($this->showLabels) {
                $formField->addItem($W->Label($labelText)
                    ->setAssociatedWidget($inputWidget));
            }
            $formField->addItem($inputWidget);
            if (! $this->showLabels) {
                $inputWidget->setPlaceHolder($labelText);
            }
        }
        if (isset($descriptionText)) {
            $formField->addItem($W->Label($descriptionText)
                ->addClass('app-field-description'));
        }
        
        return $formField;
    }
    
    /**
     * Initializes the widget before display().
     *
     * @ignore
     */
    protected function initialize()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $componentApp = $this->addressComponent;
        
        if ($this->showRecipient) {
            $recipientFormItem = $this->formField($componentApp->translate('Recipient name'), $W->TextEdit()
                ->setColumns(54)
                ->setLines(2)
                ->setName('recipient')
                ->addClass('fn widget-fullwidth'));
        } else {
            $recipientFormItem = null;
        }
        
        $streetFormItem = $this->formField($componentApp->translate('Number / pathway'), $W->TextEdit()
            ->setColumns(58)
            ->setLines(2)
            ->setName('street')
            ->setMandatory($this->mandatoryStreet, $componentApp->translate('The address street is mandatory'))
            ->addClass('street-address widget-fullwidth widget-autoresize'));
        
        $postalCodeInput = $Ui->SuggestPostalCode()
        ->setSize(4)
        ->setMandatory($this->mandatoryPostalCode, $componentApp->translate('The address postal code is mandatory'));
        $cityInput = $Ui->SuggestPlaceName()
        ->setSize(24)
        ->setMandatory($this->mandatoryCity, $componentApp->translate('The address city is mandatory'));
        if (isset($this->convertCase)) {
            $cityInput->setConvertCase($this->convertCase);
        }
        $postalCodeInput->setRelatedPlaceName($cityInput);
        $cityInput->setRelatedPostalCode($postalCodeInput);
        
        $postalCodeFormItem = $this->formField($componentApp->translate('Postal Code'), $postalCodeInput->addClass('postal-code')
            ->setName('postalCode'));
        $cityFormItem = $this->formField($componentApp->translate('City'), $cityInput->setName('city')
            ->addClass('locality'));
        $cityComplementFormItem = null;
        if ($this->showComplement) {
            $cityComplementFormItem = $this->formField($componentApp->translate('Complement'), $W->LineEdit()
                ->setSize(6)
                ->setName('cityComplement')
                ->setMandatory($this->mandatoryComplement, $componentApp->translate('The complement is mandatory')));
        }
        
        $this->setLayout($W->HBoxItems($innerAddressFrame = $W->VBoxItems($recipientFormItem, $streetFormItem, $W->HBoxItems($postalCodeFormItem, $cityFormItem, $cityComplementFormItem)
            ->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('bottom'))
            ->setVerticalSpacing(1, 'em'), 
            ($this->showMapPreview ? $W->LabelledWidget(
                $componentApp->translate('Map preview'), 
                $W->Html('<iframe class="app-address-map"  scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>')
                ->setSizePolicy(\Widget_SizePolicy::MINIMUM)
            ) : null))
            ->setVerticalAlign('top')
            ->setHorizontalSpacing(1, 'em')
            ->addClass('app-address-form widget-100pc'));
        
        if ($this->showCountry) {
            
            $countrySet = $App->CountrySet();
            $countryInput = $W->Select();
            $options = $countrySet->getOptions();
            
            if ($this->mandatoryCountry) {
                $countryInput->setMandatory(true, $componentApp->translate('The country is mandatory'));
                unset($options[0]);
            }
            
            $countryInput->setOptions($options);
            
            if (null !== $this->defaultCountry) {
                $countryInput->setValue($this->defaultCountry);
            }
            
            $countryFormItem = $this->formField($componentApp->translate('Country'), $countryInput->setName('country')
                ->addClass('country'));
            
            $innerAddressFrame->addItem($countryFormItem);
        }
        
        if ($this->showCoordones) {
            $coordonesFormItem = $W->HBoxItems($this->formField($componentApp->translate('Latitude'), $W->LineEdit()
                ->setSize(20)
                ->setName('latitude')
                ->addClass('latitude')
                ->setTitle($componentApp->translate('Automatically fill if empty with address coordinates'))), $this->formField($componentApp->translate('Longitude'), $W->LineEdit()
                    ->setSize(20)
                    ->setName('longitude')
                    ->addClass('longitude')
                    ->setTitle($componentApp->translate('Automatically fill if empty with address coordinates'))))
                    ->setHorizontalSpacing(1, 'em');
                    $innerAddressFrame->addItem($coordonesFormItem);
        }
        
        if ($this->showInstructions) {
            $innerAddressFrame->addItem($W->LabelledWidget($componentApp->translate('Additional instructions'), $W->TextEdit()
                ->setColumns(54)
                ->setLines(2)
                ->setName('instructions')
                ->addClass('widget-fullwidth')));
        }
    }
    
    public function test()
    {
        return $this->item;
    }
}