<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



namespace Capwelton\App\Address\Ui;


use Capwelton;

class AddressUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * @return null
     */
    public function tableView($id = null)
    {
        return null;
    }
    
    /**
     * @return AddressEdit
     */
    public function editor($id = null, \Widget_Layout $layout = null)
    {
        return $this->AddressEditor($id, $layout);
    }
    
    public function AddressEditor($id = null, \Widget_Layout $layout = null)
    {
        return new AddressEdit($this->app, $id, $layout);
    }
    
    /**
     * 
     * @param \Func_App $app
     * @param \app_Record $tags
     * @param \app_Record $associatedObject
     * @param \Widget_Action $tagAction
     * @param int $id
     * @return 
     */
    public function display($app, $address, \app_Record $associatedObject, \Widget_Action $tagAction = null, $id = null){
        return new AddressFrame($address,$id);
    }
    
    /**
     * @return \Widget_SuggestPostalCode
     */
    public function SuggestPostalCode($id = null)
    {
        $W = bab_Widgets();
        $suggest = $W->SuggestPostalCode($id);
        $suggest->setSuggestAction($this->app->Controller()->Address()->suggestPostalCode(), 'search');
        
        return $suggest;
    }
    
    
    /**
     * @return \Widget_SuggestPlaceName
     */
    public function SuggestPlaceName($id = null)
    {
        $W = bab_Widgets();
        $suggest = $W->SuggestPlaceName($id);
        $suggest->setSuggestAction($this->app->Controller()->Address()->suggestPlaceName(), 'search');
        
        return $suggest;
    }
}

/**
 * Display the tags associated to a app_Record.
 *
 * @extends Widget_FlowLayout
 */
function app_displayTags(\app_Record $record, \Widget_Action $tagAction = null, $id = null)
{
	$W = bab_Widgets();

	$App = $record->App();

	$tagSet = $App->TagSet();
	$tags = $tagSet->selectFor($record);

	$layout = $W->FlowLayout($id);
	$layout->setSpacing(2, 'px');

	foreach ($tags as $tag) {

		/* @var $tag Capwelton\App\Entry\Set\Tag */
		if ($tag instanceof \app_Link) {
			$tag = $tag->targetId;
		}

		if (isset($tagAction)) {
			$tagAction->setParameter('tag', $tag->label);
			$tagLabel = $W->Link($tag->label, $tagAction);
		} else {
			$tagLabel = $W->Label($tag->label);
		}
		$tagLabel->setTitle($tag->description);

		$tagDeleteLink = $W->Link(
			$W->Icon('', \Func_Icons::ACTIONS_EDIT_DELETE),
			$App->Controller()->Tag()->unlink($tag->id, $record->getRef())
		)->setAjaxAction($App->Controller()->Tag()->unlink($tag->id, $record->getRef()), $tagLabel);

		$tagLayout = $W->FlowItems(
			$tagLabel,
			$tagDeleteLink
		)
		->setVerticalAlign('middle')
		->addClass('app-tag')->addClass('icon-left-16 icon-16x16 icon-left-16');

		if (!$tag->checked) {
			$tagLayout->addClass('app-not-checked');
		}
		$layout->addItem(
			$tagLayout
		);
	}

	return $layout;
}



