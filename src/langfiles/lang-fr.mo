��          �   %   �      @     A  4   Y     �     �  
   �  _   �          8     @     I  	   R     \     h     w     �     �     �     �     �  $   �          !     =     V  #  _     �  =   �     �     �     �  p   	  %   z     �     �     �  	   �     �     �     �     �               1     >  $   ^     �  $   �     �     �                                        
                	                                                                      Additional instructions Automatically fill if empty with address coordinates City City complement (CEDEX...) Complement Countries database could not be initialized. Please ensure the library libgeonames is installed Countries database initialized Country ISO code Latitude Longitude Map preview Name (english) Name (french) Number / pathway Postal Code Recipient name State/Region The address city is mandatory The address postal code is mandatory The address street is mandatory The complement is mandatory The country is mandatory Zip code Project-Id-Version: appaddress
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-27 12:30+0200
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate;translatable;translatable:1,2
X-Generator: Poedit 3.1
X-Poedit-SearchPath-0: .
 Instructions suplémentaires Renseigné automatiquement avec les coordonnées de l'adresse Ville Complément (CEDEX, …) Complément La base de données des pays n'a pas pu être instanciée. Vérifiez que la librairie libgeonames est installée Base de données des pays instanciée Pays Code ISO Latitude Longitude Aperçu de la carte Nom (anglais) Nom (français) Numéro / Libellé de la voie Code postal Nom du destinataire Etat/Région La ville doit être spécifiée Le code postal doit être spécifié La rue doit être spécifiée Le complément doit être spécifié Le pays doit être spécifié Code ZIP 