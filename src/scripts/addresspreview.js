jQuery(document).ready(function() {
	jQuery('iframe.app-address-map:not(.app-address-map-initialized)').each(function () {
		jQuery(this).addClass('app-address-map-initialized');
		var imageAddressMap = this;
		var adr = jQuery(this).closest('.app-address-form');
		
		jQuery(adr).find('textarea,input,select').change(function () {
			var street = jQuery(adr).find('.street-address').not('.resize-clone').val();
			var postalCode = jQuery(adr).find('.postal-code').val();
			var city = jQuery(adr).find('.locality').val();
			var latitude = jQuery(adr).find('.latitude').val();
			var longitude = jQuery(adr).find('.longitude').val();
			var country = jQuery(adr).find('.country option:selected').text();
			
			var zoom = 15;
			if (street == "") {
				zoom = 10;
				if (postalCode == "" && city == "") {
					zoom = 3;
					if (country == "") {
						zoom = 1;
					}
				}
			}
			
			console.log('changed');
			
			street = cleanAddressStringPart(escape(street));
			postalCode = cleanAddressStringPart(escape(postalCode));
			city = cleanAddressStringPart(escape(city));
			country = cleanAddressStringPart(escape(country));

			var width = jQuery(imageAddressMap).width();
			var height = jQuery(imageAddressMap).height();
			
			var addressParts = [street, postalCode, city, country].filter(Boolean).join(",");
			
			var url = "https://maps.google.com/maps?width=100%25&height=" + height + "&hl=en&q=" + addressParts + "&t=&z=" + zoom + "&ie=UTF8&iwloc=B&output=embed"
			imageAddressMap.src = url;
		});
		jQuery(adr).find('textarea').change();
	});
});

function cleanAddressStringPart(text)
{
	try {
		text = text.replace(/%C8|%C9|%CA|%CB/g, "E");
		text = text.replace(/%E8|%E9|%EA|%EB/g, "e");

		text = text.replace(/%CC|%CD|%CE|%CF/g, "I");
		text = text.replace(/%EC|%ED|%EE|%EF/g, "i");

		text = text.replace(/%C0|%C1|%C2|%C3|%C4/g, "A");
		text = text.replace(/%E0|%E1|%E2|%E3|%E4/g, "a");

		text = text.replace(/%D2|%D3|%D4|%D5|%D6/g, "O");
		text = text.replace(/%F2|%F3|%F4|%F5|%F6/g, "o");

		text = text.replace(/%D9|%DA|%DB|%DC/g, "U");
		text = text.replace(/%F9|%FA|%FB|%FC/g, "u");

		text = text.replace(/%D1/g, "N");
		text = text.replace(/%F1/g, "n");

		text = text.replace(/%C7/g, "C");
		text = text.replace(/%E7/g, "c");
	} catch (e) {
		text = '';
	}
	return text;
};