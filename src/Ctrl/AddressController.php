<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Address\Ctrl;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on addresses.
 */
class AddressController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Address'));
    }
    
    /**
     * @param string $search
     *
     * @return void
     */
    public function suggestPostalCode($search = null)
    {
        $W = bab_Widgets();
        
        $suggest = $W->SuggestPostalCode();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();
        
        die;
    }
    
    /**
     * @param string $search
     *
     * @return void
     */
    public function suggestPlaceName($search = null)
    {
        $W = bab_Widgets();
        
        $suggest = $W->SuggestPlaceName();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();
        
        die;
    }
    
    public function search($q = null,$linkedRef = null,$linkType = null,$includeDataGouv = true){
        $App = $this->App();
        $set = $this->getRecordSet();
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
//         if(strlen($q)<3){
//             $collection->addOption(new \Widget_Select2Option(0, $App->translate("The research is too short")));
//             header("Content-type: application/json; charset=utf-8");
//             echo $collection->output();
//             die();
//         }
        if(isset($linkedRef)){
            $refObject = $App->getRecordByRef($linkedRef);
            $linkSet = $App->LinkSet();
            $linkSet->joinSource($refObject->getClassName());
            $linkSet->joinTarget($set->getTableName());
            
            $linkSet->targetId->country();
            
            $linkSet->targetId->addFields(
                $linkSet->targetId->street->concat(', ')
                ->concat($linkSet->targetId->postalCode)
                ->concat(' ')
                ->concat($linkSet->targetId->city)
                ->concat(', ')
                ->concat($linkSet->targetId->country->name_fr)
                ->setName('fullAddress')
                );
            if(isset($linkType)){
                $conditions = array(
                    $linkSet->sourceClass->is($refObject->getClassName()),
                    $linkSet->targetClass->is($set->getTableName()),
                    $linkSet->sourceId->is($refObject->id),
                    $linkSet->any(
                        $linkSet->targetId->fullAddress->contains($q),
                        $linkSet->targetId->street->contains($q),
                        $linkSet->targetId->country->name_fr->contains($q),
                        $linkSet->targetId->city->contains($q),
                        $linkSet->targetId->postalCode->startsWith($q)
                        ),
                    $linkSet->type->is("hasAddressType:".$linkType)->_OR_(
                        $linkSet->type->is("hasAddressType:".$App->translate($linkType))
                        )
                );
                
            }else{
                $conditions = array(
                    $linkSet->sourceClass->is($refObject->getClassName()),
                    $linkSet->targetClass->is($set->getTableName()),
                    $linkSet->sourceId->is($refObject->id),
                    $linkSet->any(
                        $linkSet->targetId->fullAddress->contains($q),
                        $linkSet->targetId->street->contains($q),
                        $linkSet->targetId->country->name_fr->contains($q),
                        $linkSet->targetId->city->contains($q),
                        $linkSet->targetId->postalCode->startsWith($q),
                        $linkSet->type->contains($q)
                        )
                );
            }
            
            
            $items = $linkSet->select($linkSet->all($conditions));
            $items->orderAsc($linkSet->sourceId->name);
            foreach($items as $i){
                $address = $i->targetId;
                $organization = $i->sourceId;
                $collection->addOption(
                    new \Widget_Select2Option(
                        $address->id,
                        $address->getRecordTitle(),
                        $organization->name . ' (' . substr($i->type, strlen('hasAddressType:')).")"
                        )
                    );
            }
        }else{
            $items = $set->select(
                $set->any(
                    $set->street->contains($q),
                    $set->city->contains($q),
                    $set->postalCode->contains($q),
                    $set->cityComplement->contains($q),
                    $set->state->contains($q)
                    )
                );
            foreach($items as $i){
                $collection->addOption(
                    new \Widget_Select2Option(
                        $i->id,
                        $i->getRecordTitle()
                        )
                    );
            }
        }
        
        if($includeDataGouv){
            $geocoder = "https://api-adresse.data.gouv.fr/search/?q=%s";
            $query = sprintf($geocoder, urlencode(bab_convertStringFromDatabase($q, 'UTF-8')));
            $opts = array('http' => array('header' => "User-Agent: Ovidentia\r\n"));
            $context = stream_context_create($opts);
            $results = file_get_contents($query, false, $context);
            $results = json_decode($results, true);
            $features = $results['features'];
            $i = 0;
            foreach ($features as $feature) {
                $i++;
                if($i == 10){
                    break;
                }
                $properties = $feature['properties'];
                $adr = $properties['name'] . '|' . $properties['postcode'] . '|' . $properties['city'].'|france';
                $collection->addOption(new \Widget_Select2Option(
                    bab_getStringAccordingToDataBase($adr, \bab_charset::UTF_8),
                    bab_getStringAccordingToDataBase($properties['name'], \bab_charset::UTF_8) . ', ' . $properties['postcode'] . ' ' . bab_getStringAccordingToDataBase($properties['city'], \bab_charset::UTF_8) . ', France',
                    'data gouv',
                    ''
                    )
                    );
            }
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
}